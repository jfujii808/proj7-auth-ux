# Project 7: Adding authentication and user interface to brevet time calculator service
Author: Jonathan Fujii 
Contact Address: jfujii@oregon.edu 
Description: This adds authentication and an user interface to the brevet time calculator. 
User Instructions: 
1) Enter information via the provided forms such as username and password to access application
2) Enter information into forms relevant for application use upon login
Any contextual information is provided by existing comments

